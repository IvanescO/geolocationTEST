/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Dimensions,
  Alert,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Icon,
} from "native-base";

import BackgroundGeolocation from "react-native-mauron85-background-geolocation";

const instructions = Platform.select({
  ios: "Press Cmd+R to reload,\n" + "Cmd+D or shake for dev menu",
  android:
    "Double tap R on your keyboard to reload,\n" +
    "Shake or press menu button for dev menu",
});

type Props = {};
export default class App extends Component<Props> {
  state = {
    region: null,
    locations: [],
    stationaries: [],
    isRunning: false,
  };
  componentDidMount() {
    BackgroundGeolocation.configure({
      desiredAccuracy: BackgroundGeolocation.HIGH_ACCURACY,
      stationaryRadius: 50,
      distanceFilter: 50,
      startForeground: false,
      notificationTitle: "Background tracking",
      notificationText: "enabled",
      debug: true,
      startOnBoot: true,
      stopOnTerminate: false,
      locationProvider: BackgroundGeolocation.ACTIVITY_PROVIDER,
      interval: 10000,
      fastestInterval: 5000,
      activitiesInterval: 10000,
      stopOnStillActivity: false,
      url: "http://192.168.81.15:3000/location",
      httpHeaders: {
        "X-FOO": "bar",
      },
      // customize post properties
      postTemplate: {
        lat: "@latitude",
        lon: "@longitude",
        foo: "bar", // you can also add your own properties
      },
    });
    BackgroundGeolocation.on("start", () => {
      // service started successfully
      // you should adjust your app UI for example change switch element to indicate
      // that service is running
      console.log("[DEBUG] BackgroundGeolocation has been started");
      this.setState({ isRunning: true });
    });
    BackgroundGeolocation.on("stop", () => {
      console.log("[DEBUG] BackgroundGeolocation has been stopped");
      this.setState({ isRunning: false });
    });
    BackgroundGeolocation.on("authorization", status => {
      console.log(
        "[INFO] BackgroundGeolocation authorization status: " + status
      );
      if (status !== BackgroundGeolocation.AUTHORIZED) {
        // we need to set delay after permission prompt or otherwise alert will not be shown
        setTimeout(
          () =>
            Alert.alert(
              "App requires location tracking",
              "Would you like to open app settings?",
              [
                {
                  text: "Yes",
                  onPress: () => BackgroundGeolocation.showAppSettings(),
                },
                {
                  text: "No",
                  onPress: () => console.log("No Pressed"),
                  style: "cancel",
                },
              ]
            ),
          1000
        );
      }
    });
    BackgroundGeolocation.on("error", ({ message }) => {
      Alert.alert("BackgroundGeolocation error", message);
    });
    BackgroundGeolocation.on("location", location => {
      console.log("[DEBUG] BackgroundGeolocation location", location);
      BackgroundGeolocation.startTask(taskKey => {
        requestAnimationFrame(() => {
          const longitudeDelta = 0.01;
          const latitudeDelta = 0.01;
          const region = Object.assign({}, location, {
            latitudeDelta,
            longitudeDelta,
          });
          const locations = this.state.locations.slice(0);
          locations.push(location);
          this.setState({ locations, region });
          BackgroundGeolocation.endTask(taskKey);
        });
      });
    });

    BackgroundGeolocation.on("foreground", () => {
      console.log("[INFO] App is in foreground");
    });

    BackgroundGeolocation.on("background", () => {
      console.log("[INFO] App is in background");
    });

    BackgroundGeolocation.checkStatus(({ isRunning }) => {
      this.setState({ isRunning });
    });
  }
  componentWillUnmount() {
    BackgroundGeolocation.events.forEach(event =>
      BackgroundGeolocation.removeAllListeners(event)
    );
  }

  toggleTracking() {
    BackgroundGeolocation.checkStatus(
      ({ isRunning, locationServicesEnabled, authorization }) => {
        if (isRunning) {
          BackgroundGeolocation.stop();
          return false;
        }

        if (!locationServicesEnabled) {
          Alert.alert(
            "Location services disabled",
            "Would you like to open location settings?",
            [
              {
                text: "Yes",
                onPress: () => BackgroundGeolocation.showLocationSettings(),
              },
              {
                text: "No",
                onPress: () => console.log("No Pressed"),
                style: "cancel",
              },
            ]
          );
          return false;
        }

        if (authorization == 99) {
          // authorization yet to be determined
          BackgroundGeolocation.start();
        } else if (authorization == BackgroundGeolocation.AUTHORIZED) {
          // calling start will also ask user for permission if needed
          // permission error will be handled in permisision_denied event
          BackgroundGeolocation.start();
        } else {
          Alert.alert(
            "App requires location tracking",
            "Please grant permission",
            [
              {
                text: "Ok",
                onPress: () => BackgroundGeolocation.start(),
              },
            ]
          );
        }
      }
    );
  }
  renderLocation = () => {
    return this.state.locations.map(e => {
      return <Text>{e.longitude}</Text>;
    });
  };
  render() {
    const { height, width } = Dimensions.get("window");
    const { locations, stationaries, region, isRunning } = this.state;
    return (
      <Container>
        <View>{this.renderLocation()}</View>

        <Footer style={styles.footer}>
          <FooterTab>
            <Button onPress={this.toggleTracking}>
              <Icon name={isRunning ? "pause" : "play"} style={styles.icon} />
            </Button>
            <Button onPress={this.goToSettings}>
              <Icon name="menu" style={styles.icon} />
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF",
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10,
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5,
  },
});
